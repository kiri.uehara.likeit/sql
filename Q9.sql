SELECT
    item.item_price AS total_price
    item_category.category_name,
FROM
    item
INNER JOIN
    item_category
ON
    item.item_id = item_category.category_name
ORDER BY
    total_price DESC;
